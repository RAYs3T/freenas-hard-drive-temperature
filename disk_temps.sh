#!/bin/bash

# List of devices to poll smart data from
SEARCH_PATTERN=( '/dev/ada?' '/dev/da?' '/dev/nvme?' '/dev/sd?' )

# Loop trough all the patterns that we've defined for smart lookup
for pattern in "${SEARCH_PATTERN[@]}"; do
    # Lets do a dir with each pattern, this will give us the fully path of each device.
    for device in $pattern; do
        # Execute the smartctrl command for the defined device.
        smart_result=$(smartctl -a "$device")
        if [ $? -eq "0" ]
        then
            # Now we've the entire smart string, lets grab the information we're interested in...
            temp_celsius=$(echo "$smart_result" | awk '/Temperature_Celsius/{print $0}' | awk '{print $10}')
            if [[ "$temp_celsius" -eq "" ]]; then
                # First way of getting the temperature did not work..
                # lets try something else ...
                temp_celsius=$(echo "$smart_result" | awk '/Temperature:/{print $0}' | awk '{print $2}')
            fi

            # If the temperature is still not set we don't know how to get it.
            # or.. the drive simply does not have a temperature sensor...
            if [[ "$temp_celsius" -eq "" ]]; then
                >&2 echo "Error not sure how to extract temperature from from smartctrl result."
            fi
            # Device Model:
            device_model=$(echo "$smart_result" | awk '/Device Model:/{print $0}' | awk '{for (i=3; i<NF; i++) printf $i " "; print $NF}')
            if [[ "$device_model" == "" ]]; then
                device_model=$(echo "$smart_result" | awk '/Model Number:/{print $0}' | awk '{for (i=3; i<NF; i++) printf $i " "; print $NF}')
            fi

            device_serial=$(echo "$smart_result" | awk '/Serial Number:/{print $0}' | awk '{for (i=3; i<NF; i++) printf $i " "; print $NF}')
            echo -e "$device_model \t (S/N: $device_serial) \t $device: \t $temp_celsius °C"
        else
            >&2 echo "Error fetching smartctl for device: $device"
        fi
    done
done
